<?php

use \VanillaModuleName\VtUtils;

class RedooSalesReport_ComplexeCondition_Action extends Vtiger_Action_Controller {

    function checkPermission(Vtiger_Request $request) {
        return;
    }

    function __construct() {
        parent::__construct();
        $this->exposeMethod('ConditionStore');
    }

    function process(Vtiger_Request $request)
    {
        $mode = $request->getMode();
        if (!empty($mode)) {
            echo $this->invokeExposedMethod($mode, $request);
            return;
        }
    }

    public function ConditionStore(Vtiger_Request $request)
    {
		global $root_directory;
		require_once($root_directory."/modules/VanillaModuleName/lib/VanillaModuleName/ComplexeCondition.php");

        $task = $request->get('task');

        if(!empty($task['condition'])) {
            $preset = new \VanillaModuleName\ComplexeCondition('condition', null, array());

            $condition = $preset->getCondition($task['condition']);
            $text = $preset->getHTML($condition, $task['module']);
        } else {
            $condition = '';
            $text = '';
        }

        echo VtUtils::json_encode(array('condition' => base64_encode(VtUtils::json_encode(array('condition' => $condition, 'module' => $task['module']))), 'html' => nl2br($text)));
    }
}

