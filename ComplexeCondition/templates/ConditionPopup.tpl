
<div class="modelContainer" style="width:1100px;">
    <form method="POST" id="PopupConditionForm" action="index.php?module={$ConditionScopeModule}&parent=Settings&action=ConditionPopupStore">
        <input type="hidden" name="task[module]" value="{$toModule}" />
<div class="modal-header contentsBackground">
	<button class="close" aria-hidden="true" data-dismiss="modal" type="button" title="{vtranslate('LBL_CLOSE')}">x</button>
    <h3>{vtranslate('configure Condition', $ConditionScopeModule)}</h3>
</div>
    <p style="margin:5px 10px;">
        {$title}
    </p>
        <div style="margin:0 10px;">
    {$conditionalContent}
        </div>

    <div class="modal-footer quickCreateActions">
            <a class="cancelLink cancelLinkContainer pull-right" type="reset" data-dismiss="modal">{vtranslate('LBL_CLOSE', $ConditionScopeModule)}</a>
        <button class="btn btn-success" type="submit" id="submitPopupCondition" ><strong>{vtranslate('store condition', $ConditionScopeModule)}</strong></button>
   	</div>
    </form>
</div>


<script type="text/javascript">
    {$javascript}
</script>
