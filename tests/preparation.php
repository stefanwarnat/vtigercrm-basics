<?php

namespace {
	require_once("../Core Structure/lib/VanillaModuleName/VtUtils.php");
	require_once("../Core Structure/lib/VanillaModuleName/VTEntity.php");
	require_once("../Core Structure/lib/VanillaModuleName/ExpressionParser.php");
	require_once("../Core Structure/lib/VanillaModuleName/VTTemplate.php");


	global $context;
	global $current_user;
			$user = new StdClass();
			$user->id = 1;
			$user->is_admin = 'on';
			$current_user = $user;
			
			\VanillaModuleName\VTEntity::setUser($user);
			$context = \VanillaModuleName\VTEntity::getForId(1, "Contacts", $user);
			$context->loadEnvironment(array("testVal" => "asdEnvVal", "testArrayVal" => array("first" => "firstval", "second" => "secondval")));
			
	global $adb;
	$adb = new Database();
			
	function vtws_retrieve($id, $user) {
		if($id == 1) {
			return array("lastname" => "mustermann", "firstname" => "max", "birthdate" => "1986-04-23", 'linkedfield' => 2);
		}
		if($id == 2) {
			return array("accountname" => "linked account");
		}
		
	}
	function vtws_getWebserviceEntityId($module_name, $id) {
		return "1x1";
	}
	function getExpressionResult($expression, $debug = false, $timeout = 30) {
		global $context;

		$parser = new \VanillaModuleName\ExpressionParser($expression, $context, $debug);
		$parser->setTimeout($timeout);
		$parser->run();
		return $parser->getReturn();
	}
	function getTemplateResult($template) {
		global $context;

		return \VanillaModuleName\VTTemplate::parse($template, $context);
	}

	define("E_NONBREAK_ERROR", 1);
	class Users {
		public static function getActiveAdminUser() {
			global $current_user;
			return $current_user;
		}	
	}
	class Database { 
		public $lastResult = array();

		public function pquery($sql, $args) {
			return $this->query($sql);
		}
		public function query($sql) {
			switch($sql) {
				case 'SELECT setype FROM vtiger_crmentity WHERE crmid = 2':
					$this->lastResult = array(
						array(
							'setype' => 'Accounts',
						)
					);
				break;
			}
		}
		
		public function query_result($dm1, $index, $col) {
			return $this->lastResult[$index][$col];
		}
	}
	class PearDatabase extends Database {}
	
	class CRMEntity { 
		public $column_fields = array();
		
		public static function getInstance() {
			return new self();
		}
		
		public function retrieve_entity_info() {
			$this->column_fields = vtws_retrieve($this->id, 'Users');
		}
	}

	class Vtiger_Module_Model {
		public static function getInstance() {
			return new Vtiger_Module_Model();
		}
	}


}