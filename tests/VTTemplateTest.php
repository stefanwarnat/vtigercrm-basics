<?php
use PHPUnit\Framework\TestCase;

chdir(dirname(__FILE__)."/");

require_once("preparation.php");

class VTTemplateTest extends TestCase
{
	public function testSimpleVariables()
    {
		$this->assertEquals("Static Text", getTemplateResult('Static Text'));
		$this->assertEquals("mustermann", getTemplateResult('$lastname'));
		$this->assertEquals("max mustermann", getTemplateResult('$firstname $lastname'));
		$this->assertEquals("2", getTemplateResult('$linkedfield'));
	
	}
	
	public function testSimpleReferenceVariables() {
		
		// Combination Reference, Static, Default Var
		$this->assertEquals("linked account Combine mustermann", getTemplateResult('$(linkedfield: (Accounts) accountname) Combine $lastname'));
		
		// Wrong reference
		$this->assertEquals("$(linked field: (Accounts) accountname) Combine mustermann", getTemplateResult('$(linked field: (Accounts) accountname) Combine $lastname'));
	}
	
	public function testBracketVariables() {
		$this->assertEquals("max mustermann", getTemplateResult('{$firstname} $lastname'));
		$this->assertEquals("linked account", getTemplateResult('{$(linkedfield: (Accounts) accountname)}'));
		$this->assertEquals("linked account mustermann", getTemplateResult('{$(linkedfield: (Accounts) accountname)} $lastname'));
	}
	
	public function testModified() {
		$this->assertEquals("MAX mustermann", getTemplateResult('{$firstname|strtoupper} $lastname'));
		$this->assertEquals("MAX", getTemplateResult('{$firstname|strtoupper|replace:m:w}'));
		
		$this->assertEquals("wAX", getTemplateResult('{$firstname|strtoupper|replace:M:w}'));
		$this->assertEquals("wAX", getTemplateResult('{$firstname|strtoupper|replace:"M":"w"}'));
		$this->assertEquals("wAX", getTemplateResult('{$firstname|strtoupper|replace:M:"w"}'));
		$this->assertEquals("Wax", getTemplateResult('{$firstname|strtoupper|replace:M:w|strtolower|ucfirst}'));

		$this->assertEquals(md5("Wax"), getTemplateResult('{$firstname|strtoupper|replace:M:w|strtolower|ucfirst|md5}'));
	}
	
	public function testExpressions() {
		$this->assertEquals("max", getTemplateResult('<?php return $firstname; ?>'));
		$this->assertEquals("max mustermann", getTemplateResult('${ return $firstname." ".$lastname; }}>'));
	}
}
?>