<?php
use PHPUnit\Framework\TestCase;

chdir(dirname(__FILE__)."/");

require_once("preparation.php");

class ExpressionEngineTest extends TestCase
{
    public function testString()
    {
		$this->assertEquals("asd123", getExpressionResult("return 'asd123';", false));
		$this->assertEquals("asd123", getExpressionResult("return 'a'.'sd1'.'23';", false));		

    }    

	public function testVariablen()
    {
	
		$this->assertEquals("asd123", getExpressionResult('$ab = "as"; return $ab."d123";'));
		$this->assertEquals("asasd123", getExpressionResult('$ab = "as"; $bc = $ab."d1"; return $ab.$bc."23";'));
		$this->assertEquals("asd123", getExpressionResult('$ab = "as"; $bc = $ab."d1"; return $bc."2"."3";'));
		
		$this->assertEquals(3, getExpressionResult('$ab = 1; $b = 2; $ab = $ab + $b; return $ab;'));
		$this->assertEquals(3, getExpressionResult('$ab = 1; $b = 2; $ab += $b; return $ab;'));
		$this->assertEquals(8, getExpressionResult('$ab = 2; $b = 4; $ab *= $b; return $ab;'));

		$this->assertEquals(1.5, getExpressionResult('$ab = 3; $b = 1.5; $ab = $ab - $b; return $ab;'));
		
		$this->assertEquals(1.5, getExpressionResult('$ab = 3; $b = 1.5; $ab -= $b; return $ab;'));
		
		$this->assertEquals(5, getExpressionResult('$ab = 3; $ab++;$ab++; return $ab;'));
		$this->assertEquals(2, getExpressionResult('$ab = 3; $ab--; return $ab;'));
		

    }	
	

	public function _testTimeout()
    {
	
	
		try {
			$exception = false;
			getExpressionResult('$disableFunctionlist = 1; for($i = 0; $i < 3; $i++) { sleep(1); echo $i."."; } ', false, 2);
		} catch (\VtigerVanillaModule\ExpressionException $exp) {
			$exception = true;
		}
		$this->assertEquals(true, $exception, 'Timeout Exception not thrown');
	
		try {
			$exception = false;
			getExpressionResult('$disableFunctionlist = 1; for($i = 0; $i < 2; $i++) { sleep(1); echo $i."."; } ', false, 3);
		} catch (\VtigerVanillaModule\ExpressionException $exp) {
			$exception = true;
		}
		$this->assertEquals(false, $exception, 'Timeout Exception wrongly thrown');
		
	}
	

	public function testContextVars()
    {
	
		$this->assertEquals("mustermann", getExpressionResult('return $lastname;'));
		$this->assertEquals("mustermannas", getExpressionResult('return $lastname."as";'));
		$this->assertEquals("max", getExpressionResult('return $firstname;'));
		$this->assertEquals("mustermann,max", getExpressionResult('return $lastname.",".$firstname;'));
		$this->assertEquals(",mustermann,max", getExpressionResult('return ",".$lastname.",".$firstname;'));
		
		$this->assertEquals(",mustermann,max", getExpressionResult('$complete = $lastname;return ",".$complete.",".$firstname;'));
		$this->assertEquals(",mustermannmax,", getExpressionResult('$complete = $lastname.$firstname; return ",".$complete.",";'));
		
		$this->assertEquals("A", getExpressionResult('if($env["testArrayVal"]["first"] == "firstval") { return "A"; }'));

    }	
	
	public function testFunctions()
    {
	
		$this->assertEquals("AS", getExpressionResult('return strtoupper("As");'));
		$this->assertEquals("MUSTERMANN", getExpressionResult('return strtoupper($lastname);'));
		$this->assertEquals("MUSTERMANN", getExpressionResult('return strtoupper($lastname).deniedFunction("ASD".$lastname);'));
		
		$this->assertEquals("MUSTERMANNas", getExpressionResult('return strtoupper($lastname)."as";'));
		$this->assertEquals("max", getExpressionResult('return $firstname;'));
		$this->assertEquals("MUSTERMANN,max", getExpressionResult('return strtoupper($lastname).",".$firstname;'));
		$this->assertEquals(",MUSTERMANN,max", getExpressionResult('return ",".strtoupper($lastname).",".$firstname;'));
		
		$this->assertEquals(",MUSTERMANN,max", getExpressionResult('$complete = strtoupper($lastname);return ",".$complete.",".$firstname;'));
		$this->assertEquals(",MUSTERMANNmax,", getExpressionResult('$complete = strtoupper($lastname).$firstname; return ",".$complete.",";'));
		
		$this->assertEquals("mus", getExpressionResult('return substr($lastname, 0, 3);'));
		$this->assertEquals("musA", getExpressionResult('return substr($lastname, 0, 3)."A";'));

    }	
	
	public function testReturn() {
		$this->assertEquals("correct2", getExpressionResult('return "correct2";'));
		$this->assertEquals("correct2", getExpressionResult('return "cor"."rect2";'));
		$this->assertEquals(true, getExpressionResult('return $firstname == "max";'));
		$this->assertEquals(false, getExpressionResult('return $firstname == "max2";'));
		$this->assertEquals(true, getExpressionResult('return substr($firstname, 1, 1) == "a";'));
		$this->assertEquals('a', getExpressionResult('return substr($firstname, 1, 1);'));
	}
	
	public function testIf()
    {

		$this->assertEquals("dummy", getExpressionResult('if($lastname == $firstname) { return "ma"."x"; } return "dummy";'));
		$this->assertEquals("max", getExpressionResult('if($lastname == $lastname) { return "ma"."x"; } return "dummy";'));
		
		$this->assertEquals("max", getExpressionResult('if($lastname != "dummy") { return "max"; } else { return "mustermann"; }'));
		$this->assertEquals("mustermann", getExpressionResult('if($lastname == "dummy") { return "max"; } else { return "mustermann"; }'));		
		$this->assertEquals("max", getExpressionResult('if($lastname != "dummy") { $return = "max"; return $return; } else { return "mustermann"; }'));
		$this->assertEquals("mustermann", getExpressionResult('if($lastname == "dummy") { $return = "max"; return $return;  } else { return "mustermann"; }'));
		
		$this->assertEquals("mustermann123", getExpressionResult('if($lastname == "dummy") { $return = "max"; } else { $return = "mustermann"; } return $return."123";'));
		
		$this->assertEquals("mustermann123", getExpressionResult('if($lastname == "dummy") { if($firstname == "maxA") { $return = "maxWRONG"; } else { $return = "max"; } } else { if($firstname == "maxA") { $return = "mustermannWRONG"; } else { $return = "mustermann"; } } return $return."123";'));
		$this->assertEquals("mustermann123", getExpressionResult('if($lastname == "dummy") { if($firstname == "maxA") { $return = "maxWRONG"; } else { $return = "max"; } } else { if($firstname == "max") { $return = "mustermann"; } else { $return = "mustermannWRONG"; } } return $return."123";'));
		
		$this->assertEquals("max123", getExpressionResult('if($lastname == "mustermann") { if($firstname == "maxA") { $return = "maxWRONG"; } else { $return = "max"; } } else { if($firstname == "max") { $return = "mustermann"; } else { $return = "mustermannWRONG"; } } return $return."123";'));
		$this->assertEquals("max123", getExpressionResult('if($lastname == "mustermann") { if($firstname == "max") { $return = "max"; } else { $return = "maxWRONG"; } } else { if($firstname == "max") { $return = "mustermann"; } else { $return = "mustermannWRONG"; } } return $return."123";'));
		
		$this->assertEquals("max123", getExpressionResult('if($lastname == "mustermann") { if($firstname == "max") { if($firstname == "max") { if($firstname == "max") { if($firstname == "maxA") { $return = "max"; } else { if($firstname == "max") { $return = "max"; } else { $return = "maxWRONG"; } } } else { $return = "maxWRONG"; } } else { $return = "maxWRONG"; }  } else { $return = "maxWRONG"; } } else { if($firstname == "max") { $return = "mustermann"; } else { $return = "mustermannWRONG"; } } return $return."123";'));
		
		$this->assertEquals("yes", getExpressionResult('$ab = 3; $b = 3; if($ab === $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("yes", getExpressionResult('$ab = 3; $b = 3; if($ab == $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("yes", getExpressionResult('$ab = "3"; $b = 3; if($ab == $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("no", getExpressionResult('$ab = "3"; $b = 3; if($ab === $b) { return "yes"; } else { return "no"; }'));
		
		$this->assertEquals("no", getExpressionResult('$ab = 3; $b = "3"; if($ab === $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("yes", getExpressionResult('$ab = 3; $b = "3"; if(!$ab === $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("no", getExpressionResult('$ab = 3; $b = "3"; if(!!$ab === $b) { return "yes"; } else { return "no"; }'));
		$this->assertEquals("yes", getExpressionResult('$ab = 3; $b = "3"; if(!!!$ab === $b) { return "yes"; } else { return "no"; }'));
		
		$this->assertEquals("0", getExpressionResult('if(strpos($firstname, "asx") != 0) {return "1";} else {return "0";}'));
		$this->assertEquals("1", getExpressionResult('if(strpos($firstname, "ax") != 0) {return "1";} else {return "0";}'));
		
		$this->assertEquals("1", getExpressionResult('if(strpos($firstname, "ax") == 1) {return "1";} else {return "0";}'));
		$this->assertEquals("0", getExpressionResult('if(strpos($firstname, "asx") == 1) {return "1";} else {return "0";}'));
		$this->assertEquals("0", getExpressionResult('if(strpos($firstname, "ax") == 0) {return "1";} else {return "0";}'));
		
		$this->assertEquals("correct", getExpressionResult('if($firstname == "max" && $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if($firstname == "max" AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		
		$this->assertEquals("correct", getExpressionResult('if(substr($firstname, 1, 2) == "ax" AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (substr($firstname, 1, 2) == "ax" AND $firstname == "maxWrong") OR substr($firstname, 1, 2) == "ax") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (substr($firstname, 1, 2) == "ax" && $firstname == "maxWrong") OR substr($firstname, 1, 2) == "ax") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));

		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!$firstname == "maxWrong" && substr($firstname, 1, 2) == "ax") OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND $firstname == "max") OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";		'));
		
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND !$firstname == "1max") OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";		'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND !(substr($firstname, 1, 1) == "x" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";		'));
		
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND (!substr($firstname, 1, 1) == "x" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND !(substr($firstname, 1, 1) == "x" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND (!$firstname == "max2" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND !($firstname == "max2" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
		$this->assertEquals("correct", getExpressionResult('if((substr($firstname, 1, 2) == "ax1" OR (!substr($firstname, 1, 2) == "ax2" AND ($firstname == "max" OR substr($firstname, 1, 1) == "y")) OR substr($firstname, 1, 2) == "ax3") AND $lastname == "mustermann") { return "correct"; } return "wrong";'));
    }
	
	public function testDate() {
		
		$this->assertEquals("1986-04-28", getExpressionResult('$date = strtotime($birthdate);return date("Y-m-d", $date + (86400 * 5))'));
		$this->assertEquals("1986-04-28", getExpressionResult('$days = 5; $date = strtotime($birthdate);return date("Y-m-d", $date + (86400 * $days))'));
		
		$this->assertEquals("1986-05-23", getExpressionResult('$date = strtotime("next month", strtotime($birthdate)); return date("Y-m-d", $date)'));
		$this->assertEquals("1986-06-23", getExpressionResult('$date = strtotime("+2 month", strtotime($birthdate)); return date("Y-m-d", $date)'));
		
		$this->assertEquals("1986-06-23", getExpressionResult('return date("Y-m-d", strtotime("+2 month", strtotime($birthdate)));'));
		$this->assertEquals("1986-06-23", getExpressionResult('$rel = "month"; return date("Y-m-d", strtotime("+2 ".$rel, strtotime($birthdate)))'));
		
	}
	
	public function testArray() {
        $this->assertEquals(array(1,5,9), getExpressionResult('$a = array(1,5,9); return $a;'));
        $this->assertEquals(5, getExpressionResult('$a = array(1,5,9); return $a[1];'));
        $this->assertEquals(6, getExpressionResult('$env["a"]["b"]["c"] = 5;$env["a"]["b"]["c"] = $env["a"]["b"]["c"] + 1;return  $env["a"]["b"]["c"];'));
        $this->assertEquals(18, getExpressionResult('$env["a"]["b"] = 5;$env["a"]["b"]["c"] = 7;$env["a"]["zasdf"]["awawc"] = 11; $env["a"]["b"]["c"] = $env["a"]["b"]["c"] + $env["a"]["zasdf"]["awawc"];return  $env["a"]["b"]["c"];'));
		
		$this->assertEquals(3, getExpressionResult('$a = array(1,3,5);$a[] = 7;return $a[1];'));
		$this->assertEquals(7, getExpressionResult('$a = array(1,3,5);$a[] = 7;$a[] = 9;return $a[3];'));
		$this->assertEquals(9, getExpressionResult('$a = array(1,3,5);$a[] = 7;$a[] = 9;$a[] = 10;return $a[4];'));
		
		$this->assertEquals(17, getExpressionResult('$a = array();$a["b"] = array(11,13,15);$a["b"][] = 17;return $a["b"][3];'));
		
		$this->assertEquals(6, getExpressionResult('$abc = array("config" => 1, "config3" => array(1,6,9),"config2" => rand()); return $abc["config3"][1];'));
		$this->assertEquals('test25', getExpressionResult('$abc = array("config" => 1, "config3" => array(1,6,9),"config2" => rand()); $abc["config5"] = array("test" => "test2"."5", "test3" => "test5"); return $abc["config5"]["test"];'));
		
		$this->assertEquals("ASDF", getExpressionResult('$env["testresult"] = "ASDF"; return $env["testresult"];'));
		$this->assertEquals("ASDF", getExpressionResult('$env["testresult"] = "ASDF"; $env["testresult"]["asd"]; return $env["testresult"];'));
		
		$this->assertEquals("BD", getExpressionResult('$env["testresult"] = array("A", "B", "C"); $env["testresult"][3] = "D"; return $env["testresult"][1].$env["testresult"][3];'));
		$this->assertEquals("a1c1", getExpressionResult('$env["testresult"] = array("A" => "a1", "B" => "b1"); $env["testresult"]["C"] = "c1"; return $env["testresult"]["A"].$env["testresult"]["C"];'));
		
		$this->assertEquals(array("config" => "ABC", "test" => 55), getExpressionResult('return array("config" => "ABC", "test" => 55);'));
		$this->assertEquals(array (array ('name' => 'value1','label' => 'Value 1','type' => 'text','config' =>array ('default' => 'Test',),),
  array('name' => 'field_1','label' => 'Value 2','type' => 'picklist','config' => array ('default' => 'WertA
WertB
WertC')),
  array ('name' => 'field_3','label' => 'Value 3','type' => 'text'),
), getExpressionResult('$fields = array();
$fields[] = array(
   "name" =>"value1",
   "label" =>"Value 1",
   "type" => "text",
   "config" => array(
      "default" => "Test"
   )
);
$fields[] = array(
   "name" =>"field_1",
   "label" =>"Value 2",
   "type" => "picklist",
   "config" => array(
      "default" => "WertA
WertB
WertC"
   )
);
$fields[] = array(
   "name" =>"field_3",
   "label" =>"Value 3",
   "type" => "text"

);
return $fields;
'));
    }

	public function testEnvironment() {
		$this->assertEquals("asdEnvVal", getExpressionResult('return $env["testVal"];'));
		$this->assertEquals("asdEnv", getExpressionResult('return substr($env["testVal"], 0, 6);'));
		
		$this->assertEquals("firstval", getExpressionResult('return $env["testArrayVal"]["first"];'));
		$this->assertEquals("secondval", getExpressionResult('return $env["testArrayVal"][substr("secondASDF", 0, 6)];'));
	}

    public function testForeach() {
        $this->assertEquals(33, getExpressionResult('$b = array(1,6,9); $a = 1; foreach($b as $value) { $a = $a + ($value * 2); } return $a;'));
        $this->assertEquals(32, getExpressionResult('$a = 0; foreach(array(1,6,9) as $value) { $a = $a + ($value * 2); } return $a;'));
        $this->assertEquals(3, getExpressionResult('$a = 0; foreach(array(1,6,9) as $i) { $a = $a + 1; } return $a;'));
		
		$this->assertEquals("abc", getExpressionResult('$html = ""; $a = array("a", "b", "c");foreach($a as $i) { $html = $html.$i; } return $html;'));
    }
	
	public function testFor() {
		$this->assertEquals(15, getExpressionResult('$a = 0; for($i = 1;$i < 15;$i++) { $a++; } return $a;'));
		
		# maximum loop counter
		$this->assertEquals(151, getExpressionResult('$a = 0; for($i = 1;$i < 15;$i++) { $i = 0; $a++; } return $a;'));
		
		# maximum loop counter
		$this->assertEquals(31, getExpressionResult('$loopLimit = 30; $a = 0; for($i = 1;$i < 15;$i++) { $i = 0; $a++; } return $a;'));
		
		# custom increments
		$this->assertEquals(8, getExpressionResult('$a = 0; for($i = 1;$i < 15;$i=$i+2) { $a++; } return $a;'));
	}
	
	public function testComplexeReturn() {
		# custom increments
		$this->assertEquals(false, getExpressionResult('$loop = 200; return $loop <= 100;'));
		$this->assertEquals(false, getExpressionResult('$loop = 200; return $loop < 100;'));
		$this->assertEquals(true, getExpressionResult('$loop = 200; return $loop > 100;'));
		
		$this->assertEquals(false, getExpressionResult('$loop = 200; return $loop == 100;'));
		$this->assertEquals(true, getExpressionResult('$loop = 100; return $loop == 100;'));
		
		$this->assertEquals(true, getExpressionResult('$loop = 200; return $loop != 100;'));
		
		$this->assertEquals(true, getExpressionResult('$loop = 200; return $loop >= 100;'));
		$this->assertEquals(false, getExpressionResult('$loop = 50; return $loop >= 100;'));
		
		$this->assertEquals(false, getExpressionResult('$loop = 200; return $loop === 100;'));
		$this->assertEquals(true, getExpressionResult('$loop = 200; return $loop !== 100;'));
	}
	
	public function testComplex() {
        $this->assertEquals(8, strlen(getExpressionResult('$length = 8;
$characters =\'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\';
$randomString = \'\';
for ($i = 1; $i < $length; $i++) {
  $randomString .= $characters[rand(0, strlen($characters) - 1)];
}
return $randomString; ')));

        $this->assertEquals("2016-04-05", getExpressionResult('
		$loop = "2016-04-04";
		$current = strtotime($loop) + 86400;
		return date("Y-m-d", $current);
 '));
        $this->assertEquals("2016-04-05", getExpressionResult('
		$loop = "2016-04-04";
		$current = strtotime($loop);
		return date("Y-m-d", $current + 86400);
 '));

	}
	
}
?>