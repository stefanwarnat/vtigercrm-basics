<?php

require_once('autoloader.php');
VanillaModuleName\Autoloader::registerDirectory(dirname(__FILE__).'/lib');

$template = 'Fristname: $firstname';
$context = VanillaModuleName\VTEntity::getForId($crmid, $moduleName);
$result = VanillaModuleName\VTTemplate::parse($template, $context);

// Short Alternative:
$result = VanillaModuleName\VTTemplate::parse($template, $crmid);