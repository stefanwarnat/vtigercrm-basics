<?php

if(!empty($_REQUEST['stefanError']) || !empty($_COOKIE['WFDDebugMode'])) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

    global $adb;
    $adb->dieOnError = true;
}

// moduleName must be Main Module PHP Namespace
$moduleName = 'VanillaModuleName';

global $root_directory;
require_once(dirname(__FILE__)."/".$moduleName.".php");
require_once(dirname(__FILE__)."/autoloader.php");

$className = '\\'.$moduleName.'\\Autoload';
$className::register($moduleName, "~/modules/".$moduleName."/lib");

/*
Option to use custom handler for special Namespaces

\VanillaModuleName\Autoload::registerHandler("RelationProvider", function($classname, $parts) {
    $provider = strtolower(end($parts));
    $file = __DIR__ . DIRECTORY_SEPARATOR . 'extends' . DIRECTORY_SEPARATOR . 'Provider' . DIRECTORY_SEPARATOR . $provider . DIRECTORY_SEPARATOR . $provider . '.inc.php';

    if(file_exists($file)) {
        require_once($file);
    }
});
*/